resource "aws_vpc" "vpc_splunk" {
  cidr_block       = "10.10.0.0/16"
 
}
resource "aws_subnet" "subnet-1" {
  vpc_id      = aws_vpc.vpc_splunk.id
  availability_zone = var.subnet1az
  cidr_block        = "10.10.0.0/24"
  
}

resource "aws_internet_gateway" "my_igw" {
    vpc_id      = aws_vpc.vpc_splunk.id
   
}

resource "aws_route_table" "my_rt" {
    vpc_id = aws_vpc.vpc_splunk.id
    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id  = aws_internet_gateway.my_igw.id
    }
 
}

resource "aws_security_group" "my_sg" {
     
    vpc_id          = aws_vpc.vpc_splunk.id
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "80"
        to_port     = "80"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "8089"
        to_port     = "8089"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "9997"
        to_port     = "9997"
    }
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "8000"
        to_port     = "8000"
    }
      ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "514"
        to_port     = "514"
    }
    
    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}

resource "aws_route_table_association" "subnet1assoc" {
    subnet_id       = aws_subnet.subnet-1.id
    route_table_id  = aws_route_table.my_rt.id
  
}


resource "aws_instance" "Splunk" {
    ami                         =  var.splunk
    instance_type               = "t2.micro"
    subnet_id                   = aws_subnet.subnet-1.id
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.Splunk.public_ip
    }

     provisioner "remote-exec" {
    inline = [
        
          "sudo apt-get -y update",
         "sudo groupadd splunk",
         "sudo useradd -g splunk splunker",
         "sudo cp -p /etc/sudoers.orig",
         "sudo echo 'splunker ALL=(ALL) NOPASSWD:ALL'>>/etc/sudoers",
         "sudo su -splunker",
         # to download the splunk package
         "wget -O splunk-8.0.1-6db836e2fb9e-linux-2.6-amd64.deb 'https://www.splunk.com/bin/splunk/DownloadActivityServlet?architecture=x86_64&platform=linux&version=8.0.1&product=splunk&filename=splunk-8.0.1-6db836e2fb9e-linux-2.6-amd64.deb&wget=true'",
    
         # to install splunk
         "sudo dpkg -i splunk-8.0.1-6db836e2fb9e-linux-2.6-amd64.deb",
         "sudo chown -R splunker:splunk /opt/splunk" ,        
         "sudo /opt/splunk/bin/splunk start --accept-license start --accept-license --answer-yes --no-prompt --seed-passwd 'admin@123'",
         "sudo /opt/splunk/bin/splunk enable boot-start --accept-license",
         "sudo service splunk start"
         
         
    ]
  }
  
}
output "vpc_splunk_ip" {
  value = aws_instance.Splunk.public_ip
}

