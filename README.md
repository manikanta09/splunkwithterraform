## Splunk
  * Splunk is a software used to search and analyze machine data. This machine data can come from web          applications, sensors, devices or any data created by user. It has built-in features to recognize the data types, field separators and optimize the search processes. It also provides data visualization on the search results.
## Product Categories
  * Splunk is available in three different product categories
     * Splunk Enterprise
     * Splunk Cloud 
     * Splunk Light
## Splunk Installation
  * to download the setup using the below link which is available for both windows and Linux platforms.
```
    https://www.splunk.com/en_us/download/splunk-enterprise.html
```
* To download the splunk, we need to register.
 * Now, select linux os and download the __.deb__ package.
 * we will get url 
 ```
    wget -O splunk-8.0.1-6db836e2fb9e-linux-2.6-amd64.deb 'https://www.splunk.com/bin/splunk/DownloadActivityServlet?architecture=x86_64&platform=linux&version=8.0.1&product=splunk&filename=splunk-8.0.1-6db836e2fb9e-linux-2.6-amd64.deb&wget=true'
```
# Before install splunk
```
   "sudo apt-get -y update",
   
   "sudo groupadd splunk",

   "sudo useradd -g splunk splunker",
  
   "sudo cp -p /etc/sudoers.orig",
   
   "sudo echo 'splunker ALL=(ALL) NOPASSWD:ALL'>>/etc/sudoers",
   
   "sudo su -splunker",
```
     # to download the splunk package
```     
   "wget -O splunk-8.0.1-6db836e2fb9e-linux-2.6-amd64.deb 'https://www.splunk.com/bin/splunk/DownloadActivityServlet?architecture=x86_64&platform=linux&version=8.0.1&product=splunk&filename=splunk-8.0.1-6db836e2fb9e-linux-2.6-amd64.deb&wget=true'",
```
     # to install splunk
```   
   "sudo dpkg -i splunk-8.0.1-6db836e2fb9e-linux-2.6-amd64.deb",

   "sudo chown -R splunker:splunk /opt/splunk" ,        
```   
   "sudo /opt/splunk/bin/splunk start --accept-license start --accept-license --answer-yes--no-prompt--seed-passwd 'admin@123'",
```   
   "sudo /opt/splunk/bin/splunk enable boot-start --accept-license",
```

   "sudo service splunk start"


Finaly you can access the Splunk Web interface at http://Server-IP:8000/ or http://Server-hostname:8000 using the default user admin. Before we forgot make sure the port 8000 is opened on your server firewall.
   